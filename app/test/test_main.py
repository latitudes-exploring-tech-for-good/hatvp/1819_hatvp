import mock
import pytest

import os
import logging
import util

import pandas as pd
# options to debug (clean printing)
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

import json
import requests
from requests.models import Response
from unittest.mock import Mock

from util.cleaning import clean_dico_df
from util.create_tables import create_cleaned_tables, create_merged_tables, create_views_tables, \
    create_psql_tables
from util.extract_json import download_raw_json, write_json_to_folder,  extract_json_to_dataframe, df_all_flattened, \
    process_df_dataframe, get_dict_columns, flatten_dict_column, process_df_dataframe

LOGGER = logging.getLogger(__name__)

def test_download_raw_json(monkeypatch):
    """
     Test downloading json file from an external server
    """
    monkeypatch.setattr(requests, 'get', mock_get)
    request = download_raw_json()
    assert request.status_code == 200


def test_write_json_to_folder():
    """
     Test writing json file on local server
    """
    request = getRequest()
    write_json_to_folder(request)
    assert open(util.extract_json.REPERTOIRE_JSON_PATH, 'rb').read() == request.content

def test_get_dict_columns():
    """
     Test finding dicts in current dataframe
    """
    dataframe = getDataFrame()
    dict_column = get_dict_columns(dataframe)
    assert "d" == dict_column[0]
    assert "e" == dict_column[1]

def test_flatten_dict_column():
    """
     Test flattening all dicts in current dataframe
    """
    dataframe = getDataFrame()
    dict_column = get_dict_columns(dataframe)
    flattened_dict_column = flatten_dict_column(dataframe, dict_column[0])
    flattened_dict_column = flatten_dict_column(flattened_dict_column, dict_column[1])
    assert 4 == flattened_dict_column["d_d1"][0]
    assert 5 == flattened_dict_column["e_e1"][0]
    assert 6 == flattened_dict_column["e_e2"][0]
    assert {"f1": 7} == flattened_dict_column["e_e3"][0]

def test_flatten_dataframe_dict():
    """
     Test flattening all dicts recursively in current dataframe
    """
    dataframe = getDataFrame()
    flattened_dataframe = df_all_flattened(dataframe)
    assert 4 == flattened_dataframe["d_d1"][0]
    assert 5 == flattened_dataframe["e_e1"][0]
    assert 6 == flattened_dataframe["e_e2"][0]
    assert 7 == flattened_dataframe["e_e3_f1"][0]

def test_process_df_dataframe():
    """
     Test flattening and converting to dataframe all lists and dicts in current dataframe
    """
    dataframe = getDataFrame()
    df = df_all_flattened(dataframe)
    final_flattened_dataframe = dict()
    process_df_dataframe(df, "level_1", final_flattened_dataframe)

    assert 4 == final_flattened_dataframe["level_1"]["d_d1"][0]
    assert 5 == final_flattened_dataframe["level_1"]["e_e1"][0]
    assert 6 == final_flattened_dataframe["level_1"]["e_e2"][0]
    assert 7 == final_flattened_dataframe["level_1"]["e_e3_f1"][0]

    assert 0 == final_flattened_dataframe["g"]["g_id"][0]
    assert 0 == final_flattened_dataframe["g"]["level_1_id"][0]

    assert 0 == final_flattened_dataframe["h"]["g_id"][0]
    assert 0 == final_flattened_dataframe["h"]["g_id"][1]
    assert 0 == final_flattened_dataframe["h"]["g_id"][2]
    assert 2 == final_flattened_dataframe["h"][0][0]
    assert 3 == final_flattened_dataframe["h"][0][1]
    assert 4 == final_flattened_dataframe["h"][0][2]

    assert 0 == final_flattened_dataframe["i"]["g_id"][0]
    assert 1 == final_flattened_dataframe["i"]["j"][0]
    assert 2 == final_flattened_dataframe["i"]["k"][0]
    assert 8 == final_flattened_dataframe["i"]["l"][1]

    assert 1 == final_flattened_dataframe["g"]["g_id"][1]
    assert 9 == final_flattened_dataframe["g"]["m"][1]

    assert 0 == final_flattened_dataframe["n"]["level_1_id"][0]
    assert 12 == final_flattened_dataframe["n"]["o_p"][0]

def test_process_df_dataframe_with_declaration():
    """
     Test flattening and converting to dataframe all lists and dicts in current dataframe with a declaration from hatvp register
    """
    expected_representants = \
        {
          'representants_id': [0],
          'adresse': ["ALLIANCE CONTRE LE TABAC\r\nESCALIER E ETAGE 4\r\n13 RUE D UZES"],
          'codePostal': ["75002"],
          'dateCreation': ["15/05/2019 10:33:02"],
          'datePremierePublication': ["15/05/2019 10:33:02"],
          'declarationOrgaAppartenance': [True],
          'declarationTiers': [False],
          'denomination': ["ALLIANCE CONTRE LE TABAC"],
          'identifiantNational': ["383552676"],
          'isActivitesPubliees': [False],
          'lienPageTwitter': ["https://twitter.com/FranceSANStabac"],
          'lienSiteWeb': ["https://www.alliancecontreletabac.org/"],
          'pays': ["FRANCE"],
          'publierMonAdresseEmail': [True],
          'publierMonAdressePhysique': [True],
          'publierMonTelephoneDeContact': [True],
          'telephoneDeContact': ["0143379151"],
          'typeIdentifiantNational': ["SIREN"],
          'ville': ["PARIS"],
          'categorieOrganisation_categorie': ["ORGA_NON_GOUV"],
          'categorieOrganisation_code': ["ASSOC"],
          'categorieOrganisation_label': ["Association"],
        }
    expected_representants_df = pd.DataFrame(expected_representants, columns=['representants_id',
            'adresse', 'codePostal', 'dateCreation', 'datePremierePublication',
            'declarationOrgaAppartenance', 'declarationTiers', 'denomination', 'identifiantNational',
            'isActivitesPubliees', 'lienPageTwitter', 'lienSiteWeb', 'pays', 'publierMonAdresseEmail',
            'publierMonAdressePhysique', 'publierMonTelephoneDeContact', 'telephoneDeContact',
            'typeIdentifiantNational', 'ville', 'categorieOrganisation_categorie',
            'categorieOrganisation_code', 'categorieOrganisation_label'])

    expected_dirigeants = \
        {
            'representants_id': [0, 0],
            'civilite':  ['M', 'MME'],
            'fonction': ['Président', 'Directrice'],
            'nom': ['JOSSERAN', 'CAGNAT LARDEAU'],
            'prenom': ['Loïc', 'Clemence'],
        }
    expected_dirigeants_df = pd.DataFrame(expected_dirigeants, columns=['civilite', 'fonction',
        'nom', 'prenom', 'representants_id'])

    expected_affiliations = \
    {
        'representants_id': [0],
        'denomination':  ['FEDERATION FRANCAISE D\'ADDICTOLOGIE'],
        'identifiantNational': ['439242058'],
        'typeIdentifiantNational': ['SIREN']
    }
    expected_affiliations_df = pd.DataFrame(expected_affiliations, columns=['denomination',
        'identifiantNational', 'typeIdentifiantNational', 'representants_id'])

    expected_collaborateurs = \
        {
            'representants_id': [0, 0],
            'civilite':  ['M', 'MME'],
            'fonction': ['Président', 'Directrice'],
            'nom': ['JOSSERAN', 'CAGNAT LARDEAU'],
            'prenom': ['Loïc', 'Clemence'],
        }
    expected_collaborateurs_df = pd.DataFrame(expected_collaborateurs, columns=['civilite',
        'fonction', 'nom', 'prenom', 'representants_id'])

    expected_clients = {'representants_id': [0],}
    expected_clients_df = pd.DataFrame(expected_clients, columns=['representants_id'])

    expected_activites_listSecteursActivites = \
        {
            'representants_id': [0, 0, 0, 0, 0],
            'code':  ['RECHERCHE', 'FINANCES', 'PUBLIC', 'SANTE', 'SOCIETE'],
            'label': ['Recherche, innovation', 'Fiscalité, finances publiques',
                      'Pouvoirs publics, institutions, fonction publique',
                      'Santé, sécurité sociale', 'Questions de société'],
            'categorie': [None, None, None, None, None],
            'ordre': [12, 13, 21, 25, 26],
        }
    expected_activites_listSecteursActivites_df = \
        pd.DataFrame(expected_activites_listSecteursActivites, columns=[
            'categorie', 'code',
            'label', 'ordre',
            'representants_id'
        ])

    expected_activites_listNiveauIntervention = \
    {
        'representants_id': [0, 0],
        'code':  ['NATIONAL', 'EUROPEEN'],
        'label': ['National', 'Européen'],
        'categorie': [None, None],
        'ordre': [2, 3],
    }
    expected_activites_listNiveauIntervention_df = \
        pd.DataFrame(expected_activites_listNiveauIntervention, columns=[
            'categorie', 'code',
            'label', 'ordre',
            'representants_id'
        ])

    expected_exercices = \
        {
            'exercices_id': [0, 1],
            'representants_id': [0, 0],
            'publicationCourante_dateDebut': ["15-05-2019", "01-01-2019"],
            'publicationCourante_dateFin': ["31-12-2019", "14-05-2019"],
            'publicationCourante_defautDeclaration': [False, False],
            'publicationCourante_exerciceId': [7913, 7916],
            'publicationCourante_noActivite': [False, False],
            'publicationCourante_nombreActivite': [0, 1],
            'publicationCourante_chiffreAffaire': [None, "< 100 000 euros"],
            'publicationCourante_hasNotChiffreAffaire': [None, False],
            'publicationCourante_montantDepense': [None, "< 10 000 euros"],
            'publicationCourante_nombreSalaries': [None, 3],
            'publicationCourante_publicationDate': [None, "15/05/2019 à 14:32:44"],
        }
    expected_exercices_df = pd.DataFrame(expected_exercices, columns=[
        'exercices_id',
        'representants_id',
        'publicationCourante_dateDebut',
        'publicationCourante_dateFin',
        'publicationCourante_defautDeclaration',
        'publicationCourante_exerciceId',
        'publicationCourante_noActivite',
        'publicationCourante_nombreActivite',
        'publicationCourante_chiffreAffaire',
        'publicationCourante_hasNotChiffreAffaire',
        'publicationCourante_montantDepense',
        'publicationCourante_nombreSalaries',
        'publicationCourante_publicationDate'
    ])

    expected_activite = \
        {
            'publicationCourante_activites_id': [0],
            'exercices_id': [1],
            'publicationCourante_identifiantFiche': ["19V0ME1H"],
            'publicationCourante_objet': ["Inclure la Déclaration des droits de l'animal dans le droit dur"],
            'publicationCourante_publicationDate': ["15/05/2019 à 14:34:56"],
        }

    expected_activite_df = pd.DataFrame(expected_activite, columns=[
        'publicationCourante_activites_id',
        'exercices_id',
        'publicationCourante_identifiantFiche',
        'publicationCourante_objet',
        'publicationCourante_publicationDate'
    ])

    expected_activite_domainesIntervention = \
        {
            0: ["Bien-être animal"],
            'publicationCourante_activites_id': [0],
        }

    expected_activite_domainesIntervention_df = pd.DataFrame(expected_activite_domainesIntervention, columns=[
        0,
        'publicationCourante_activites_id'
    ])

    expected_actionsRepresentationInteret = \
        {
            'publicationCourante_actionsRepresentationInteret_id':  [0],
            'observation':  ["Courrier adressé aux candidats tête de liste aux élections européennes pour leur demander de soutenir la Déclaration des droits de l'animal dans le but d'intégrer ses articles dans un texte de loi européen ou national."],
            'publicationCourante_activites_id':  [0],
        }

    expected_actionsRepresentationInteret_df = pd.DataFrame(expected_actionsRepresentationInteret, columns=[
        'publicationCourante_actionsRepresentationInteret_id',
        'observation',
        'publicationCourante_activites_id'
    ])

    expected_actionsRepresentationInteret_responsablesPublics = \
        {
            0: ["Député ; sénateur ; collaborateur parlementaire ou agents des services des assemblées parlementaires"],
            'publicationCourante_actionsRepresentationInteret_id': [0],
        }

    expected_actionsRepresentationInteret_responsablesPublics_df = pd.DataFrame(expected_actionsRepresentationInteret_responsablesPublics, columns=[
        0,
        'publicationCourante_actionsRepresentationInteret_id'
    ])

    expected_actionsRepresentationInteret_decisionsConcernees = \
        {
            0: ["Lois, y compris constitutionnelles"],
            'publicationCourante_actionsRepresentationInteret_id': [0],
        }

    expected_actionsRepresentationInteret_decisionsConcernees_df = pd.DataFrame(expected_actionsRepresentationInteret_decisionsConcernees, columns=[
        0,
        'publicationCourante_actionsRepresentationInteret_id'
    ])

    expected_actionsRepresentationInteret_actionsMenees = \
        {
            0: ["Transmettre aux décideurs publics des informations, expertises dans un objectif de conviction"],
            'publicationCourante_actionsRepresentationInteret_id': [0],
        }

    expected_actionsRepresentationInteret_actionsMenees_df = pd.DataFrame(expected_actionsRepresentationInteret_actionsMenees, columns=[
        0,
        'publicationCourante_actionsRepresentationInteret_id'
    ])

    expected_actionsRepresentationInteret_tiers = \
        {
            0: ["LA FONDATION DROIT ANIMAL ETHIQUE ET SCIENCES (en propre)"],
            'publicationCourante_actionsRepresentationInteret_id': [0],
        }

    expected_actionsRepresentationInteret_tiers_df = pd.DataFrame(expected_actionsRepresentationInteret_tiers, columns=[
        0,
        'publicationCourante_actionsRepresentationInteret_id'
    ])


    dataframe = getDeclarationDataframe()
    df = df_all_flattened(dataframe)
    final_flattened_dataframe = dict()
    process_df_dataframe(df, "representants", final_flattened_dataframe)

    # printDeclarationDf(final_flattened_dataframe)

    assert final_flattened_dataframe["representants"].equals(expected_representants_df)
    assert final_flattened_dataframe["dirigeants"].equals(expected_dirigeants_df)
    assert final_flattened_dataframe["affiliations"].equals(expected_affiliations_df)
    assert final_flattened_dataframe["collaborateurs"].equals(expected_collaborateurs_df)
    assert final_flattened_dataframe["clients"].equals(expected_clients_df)
    assert final_flattened_dataframe["activites_listSecteursActivites"].equals(expected_activites_listSecteursActivites_df)
    assert final_flattened_dataframe["activites_listNiveauIntervention"].equals(expected_activites_listNiveauIntervention_df)
    assert final_flattened_dataframe["exercices"].equals(expected_exercices_df)
    assert final_flattened_dataframe["publicationCourante_activites"].equals(expected_activite_df)
    assert final_flattened_dataframe["publicationCourante_domainesIntervention"].equals(expected_activite_domainesIntervention_df)
    assert final_flattened_dataframe["publicationCourante_actionsRepresentationInteret"].equals(expected_actionsRepresentationInteret_df)
    assert final_flattened_dataframe["reponsablesPublics"].equals(expected_actionsRepresentationInteret_responsablesPublics_df)
    assert final_flattened_dataframe["decisionsConcernees"].equals(expected_actionsRepresentationInteret_decisionsConcernees_df)
    assert final_flattened_dataframe["actionsMenees"].equals(expected_actionsRepresentationInteret_actionsMenees_df)
    assert final_flattened_dataframe["tiers"].equals(expected_actionsRepresentationInteret_tiers_df)

@pytest.mark.skip(reason="No need to test because it only calls methodsd we previously tested")
def test_extract_json_to_dataframe():
    assert True

@pytest.mark.skip(reason="Needs Postgresql running")
def test_create_psql_tables():
    create_psql_tables()


"""
 Utils methods for tests
"""


class Mock_Response(object):
    """
     Mock Class for request response
    """
    def __init__(self):
        self.status_code = 200
        self.content = '''
                        {
                          "publications": [
                            {
                              "a": "1", "b": "2", "c": 3
                            }
                          ]
                        }
                        '''

    def json(self):
        return json.loads(self.content)

    def raise_for_status(self):
        return None

def mock_get(url):
    return Mock_Response()

def getRequest():
    request = Mock(spec=Response)
    request.status_code = 200
    request.content = b'{"publications": [{"a": "1", "b": "2", "c": 3, "d": {"d1": 4}}]}'
    request.json.return_value = { \
        "publications": [ \
            { \
                "a": "1", "b": "2", "c": 3, "d": {"d1": 4} \
                } \
            ] \
        }
    return request

def getJson():
    request = Mock(spec=Response)
    request.status_code = 200
    request.content = b'{"publications": [{"a": "1", "b": "2", "c": 3, "d": {"d1": 4}, "e": {"e1": 5, "e2": 6, "e3": {"f1": 7}, "g": [{"h": [2, 3, 4], "i": [{"j": 1, "k": 2}, {"l": 8}]}, {"m": 9}], "n": {"o": {"p": 12}}}'
    request.json.return_value = { \
        "publications": [ \
            { \
                "a": "1", "b": "2", "c": 3, "d": {"d1": 4}, "e": {"e1": 5, "e2": 6, "e3": {"f1": 7}}, "g": [{"h": [2, 3, 4], "i": [{"j": 1, "k": 2}, {"l": 8}]}, {"m": 9}], "n": [{"o": {"p": 12}}] \
                } \
            ] \
        }
    return request.json()

def getDataFrame():
    repertoire = getJson()
    return pd.DataFrame(repertoire['publications'])

def getDeclarationJson():
    request = Mock(spec=Response)
    request.status_code = 200
    request.content = b"{\"publications\": [{ \"typeIdentifiantNational\": \"SIREN\", \"denomination\": \"ALLIANCE CONTRE LE TABAC\", \"categorieOrganisation\": { \"code\": \"ASSOC\", \"label\": \"Association\", \"categorie\": \"ORGA_NON_GOUV\" }, \"adresse\": \"ALLIANCE CONTRE LE TABAC\\r\\nESCALIER E ETAGE 4\\r\\n13 RUE D UZES\", \"publierMonAdressePhysique\": True, \"codePostal\": \"75002\", \"ville\": \"PARIS\", \"pays\": \"FRANCE\", \"publierMonAdresseEmail\": True, \"publierMonTelephoneDeContact\": True, \"telephoneDeContact\": \"0143379151\", \"lienSiteWeb\": \"https:\/\/www.alliancecontreletabac.org\/\", \"lienPageTwitter\": \"https:\/\/twitter.com\/FranceSANStabac\", \"dirigeants\": [ { \"civilite\": \"M\", \"nom\": \"JOSSERAN\", \"prenom\": \"Lo\u00EFc\", \"fonction\": \"Pr\u00E9sident\" }, { \"civilite\": \"MME\", \"nom\": \"CAGNAT LARDEAU\", \"prenom\": \"Clemence\", \"fonction\": \"Directrice\" } ], \"collaborateurs\": [ { \"civilite\": \"M\", \"nom\": \"JOSSERAN\", \"prenom\": \"Lo\u00EFc\", \"fonction\": \"Pr\u00E9sident\" }, { \"civilite\": \"MME\", \"nom\": \"CAGNAT LARDEAU\", \"prenom\": \"Clemence\", \"fonction\": \"Directrice\" } ], \"declarationTiers\": False, \"clients\": [{}], \"declarationOrgaAppartenance\": True, \"affiliations\": [ { \"denomination\": \"FEDERATION FRANCAISE D'ADDICTOLOGIE\", \"identifiantNational\": \"439242058\", \"typeIdentifiantNational\": \"SIREN\" } ], \"activites\": { \"listSecteursActivites\": [ { \"code\": \"RECHERCHE\", \"label\": \"Recherche, innovation\", \"categorie\": None, \"ordre\": 12 }, { \"code\": \"FINANCES\", \"label\": \"Fiscalit\u00E9, finances publiques\", \"categorie\": None, \"ordre\": 13 }, { \"code\": \"PUBLIC\", \"label\": \"Pouvoirs publics, institutions, fonction publique\", \"categorie\": None, \"ordre\": 21 }, { \"code\": \"SANTE\", \"label\": \"Sant\u00E9, s\u00E9curit\u00E9 sociale\", \"categorie\": None, \"ordre\": 25 }, { \"code\": \"SOCIETE\", \"label\": \"Questions de soci\u00E9t\u00E9\", \"categorie\": None, \"ordre\": 26 } ], \"listNiveauIntervention\": [ { \"code\": \"NATIONAL\", \"label\": \"National\", \"categorie\": None, \"ordre\": 2 }, { \"code\": \"EUROPEEN\", \"label\": \"Europ\u00E9en\", \"categorie\": None, \"ordre\": 3 } ] }, \"exercices\": [ { \"publicationCourante\": { \"dateDebut\": \"15-05-2019\", \"dateFin\": \"31-12-2019\", \"exerciceId\": 7913, \"noActivite\": False, \"nombreActivite\": 0, \"defautDeclaration\": False } }, { \"publicationCourante\": { \"publicationDate\": \"15\/05\/2019 \u00E0 14:32:44\", \"dateDebut\": \"01-01-2019\", \"dateFin\": \"14-05-2019\", \"chiffreAffaire\": \"< 100 000 euros\", \"hasNotChiffreAffaire\": False, \"montantDepense\": \"< 10 000 euros\", \"nombreSalaries\": 3, \"activites\": [ { \"publicationCourante\": { \"publicationDate\": \"15\/05\/2019 \u00E0 14:34:56\", \"identifiantFiche\": \"19V0ME1H\", \"objet\": \"Inclure la D\u00E9claration des droits de l'animal dans le droit dur\", \"domainesIntervention\": [ \"Bien-\u00EAtre animal\" ], \"actionsRepresentationInteret\": [ { \"reponsablesPublics\": [ \"D\u00E9put\u00E9 ; s\u00E9nateur ; collaborateur parlementaire ou agents des services des assembl\u00E9es parlementaires\" ], \"decisionsConcernees\": [ \"Lois, y compris constitutionnelles\" ], \"actionsMenees\": [ \"Transmettre aux d\u00E9cideurs publics des informations, expertises dans un objectif de conviction\" ], \"tiers\": [ \"LA FONDATION DROIT ANIMAL ETHIQUE ET SCIENCES (en propre)\" ], \"observation\": \"Courrier adress\u00E9 aux candidats t\u00EAte de liste aux \u00E9lections europ\u00E9ennes pour leur demander de soutenir la D\u00E9claration des droits de l'animal dans le but d'int\u00E9grer ses articles dans un texte de loi europ\u00E9en ou national.\" } ] } } ], \"exerciceId\": 7916, \"noActivite\": False, \"nombreActivite\": 1, \"defautDeclaration\": False } } ], \"isActivitesPubliees\": False, \"identifiantNational\": \"383552676\", \"datePremierePublication\": \"15\/05\/2019 10:33:02\", \"dateCreation\": \"15\/05\/2019 10:33:02\" }]}"
    request.json.return_value = { \
        "publications": [ \
            {
                "typeIdentifiantNational": "SIREN",
                "denomination": "ALLIANCE CONTRE LE TABAC",
                "categorieOrganisation": {
                    "code": "ASSOC",
                    "label": "Association",
                    "categorie": "ORGA_NON_GOUV"
                },
                "adresse": "ALLIANCE CONTRE LE TABAC\r\nESCALIER E ETAGE 4\r\n13 RUE D UZES",
                "publierMonAdressePhysique": True,
                "codePostal": "75002",
                "ville": "PARIS",
                "pays": "FRANCE",
                "publierMonAdresseEmail": True,
                "publierMonTelephoneDeContact": True,
                "telephoneDeContact": "0143379151",
                "lienSiteWeb": "https://www.alliancecontreletabac.org/",
                "lienPageTwitter": "https://twitter.com/FranceSANStabac",
                "dirigeants": [
                    {
                        "civilite": "M",
                        "nom": "JOSSERAN",
                        "prenom": "Loïc",
                        "fonction": "Président"
                    },
                    {
                        "civilite": "MME",
                        "nom": "CAGNAT LARDEAU",
                        "prenom": "Clemence",
                        "fonction": "Directrice"
                    }
                ],
                "collaborateurs": [
                    {
                        "civilite": "M",
                        "nom": "JOSSERAN",
                        "prenom": "Loïc",
                        "fonction": "Président"
                    },
                    {
                        "civilite": "MME",
                        "nom": "CAGNAT LARDEAU",
                        "prenom": "Clemence",
                        "fonction": "Directrice"
                    }
                ],
                "declarationTiers": False,
                "clients": [{}],
                "declarationOrgaAppartenance": True,
                "affiliations": [
                    {
                        "denomination": "FEDERATION FRANCAISE D'ADDICTOLOGIE",
                        "identifiantNational": "439242058",
                        "typeIdentifiantNational": "SIREN"
                    }
                ],
                "activites": {
                    "listSecteursActivites": [
                        {
                            "code": "RECHERCHE",
                            "label": "Recherche, innovation",
                            "categorie": None,
                            "ordre": 12
                        },
                        {
                            "code": "FINANCES",
                            "label": "Fiscalité, finances publiques",
                            "categorie": None,
                            "ordre": 13
                        },
                        {
                            "code": "PUBLIC",
                            "label": "Pouvoirs publics, institutions, fonction publique",
                            "categorie": None,
                            "ordre": 21
                        },
                        {
                            "code": "SANTE",
                            "label": "Santé, sécurité sociale",
                            "categorie": None,
                            "ordre": 25
                        },
                        {
                            "code": "SOCIETE",
                            "label": "Questions de société",
                            "categorie": None,
                            "ordre": 26
                        }
                    ],
                    "listNiveauIntervention": [
                        {
                            "code": "NATIONAL",
                            "label": "National",
                            "categorie": None,
                            "ordre": 2
                        },
                        {
                            "code": "EUROPEEN",
                            "label": "Européen",
                            "categorie": None,
                            "ordre": 3
                        }
                    ]
                },
                "exercices": [
                    {
                        "publicationCourante": {
                            "dateDebut": "15-05-2019",
                            "dateFin": "31-12-2019",
                            "exerciceId": 7913,
                            "noActivite": False,
                            "nombreActivite": 0,
                            "defautDeclaration": False
                        }
                    },
                    {
                        "publicationCourante": {
                            "publicationDate": "15/05/2019 à 14:32:44",
                            "dateDebut": "01-01-2019",
                            "dateFin": "14-05-2019",
                            "chiffreAffaire": "< 100 000 euros",
                            "hasNotChiffreAffaire": False,
                            "montantDepense": "< 10 000 euros",
                            "nombreSalaries": 3,
                            "activites": [
                                {
                                    "publicationCourante": {
                                        "publicationDate": "15/05/2019 à 14:34:56",
                                        "identifiantFiche": "19V0ME1H",
                                        "objet": "Inclure la Déclaration des droits de l'animal dans le droit dur",
                                        "domainesIntervention": [
                                            "Bien-être animal"
                                        ],
                                        "actionsRepresentationInteret": [
                                            {
                                                "reponsablesPublics": [
                                                    "Député ; sénateur ; collaborateur parlementaire ou agents des services des assemblées parlementaires"
                                                ],
                                                "decisionsConcernees": [
                                                    "Lois, y compris constitutionnelles"
                                                ],
                                                "actionsMenees": [
                                                    "Transmettre aux décideurs publics des informations, expertises dans un objectif de conviction"
                                                ],
                                                "tiers": [
                                                    "LA FONDATION DROIT ANIMAL ETHIQUE ET SCIENCES (en propre)"
                                                ],
                                                "observation": "Courrier adressé aux candidats tête de liste aux élections européennes pour leur demander de soutenir la Déclaration des droits de l'animal dans le but d'intégrer ses articles dans un texte de loi européen ou national."
                                            }
                                        ]
                                    }
                                }
                            ],
                            "exerciceId": 7916,
                            "noActivite": False,
                            "nombreActivite": 1,
                            "defautDeclaration": False
                        }
                    }
                ],
                "isActivitesPubliees": False,
                "identifiantNational": "383552676",
                "datePremierePublication": "15/05/2019 10:33:02",
                "dateCreation": "15/05/2019 10:33:02"
            }
            ] \
        }
    return request.json()

def getDeclarationDataframe():
    repertoire = getDeclarationJson()
    return pd.DataFrame(repertoire['publications'])

def printDeclarationDf(final_flattened_dataframe):
    print("---------- REPRESENTANTS ------------")
    print(final_flattened_dataframe["representants"])
    print("---------- DIRIGEANTS ------------")
    print(final_flattened_dataframe["dirigeants"])
    print("---------- AFFILIATIONS ------------")
    print(final_flattened_dataframe["affiliations"])
    print("---------- CLIENTS ------------")
    print(final_flattened_dataframe["clients"])
    print("---------- COLLABORATEURS ------------")
    print(final_flattened_dataframe["collaborateurs"])
    print("---------- ACTIVITES : NIVEAU INTERVENTION ------------")
    print(final_flattened_dataframe["activites_listNiveauIntervention"])
    print("---------- ACTIVITES : SECTEURS ACTIVITES ------------")
    print(final_flattened_dataframe["activites_listSecteursActivites"])
    print("---------- EXERCICES ------------")
    print(final_flattened_dataframe["exercices"])
    print("---------- ACTIVITES ------------")
    print(final_flattened_dataframe['publicationCourante_activites'])
    print("---------- ACTIVITES : DOMAINES INTERVENTIONS ------------")
    print(final_flattened_dataframe['publicationCourante_domainesIntervention'])
    print("---------- ACTIVITES : ACTIONS ------------")
    print(final_flattened_dataframe['publicationCourante_actionsRepresentationInteret'])
    print("---------- ACTIVITES : ACTIONS : RESPONSABLES PUBLICS ------------")
    print(final_flattened_dataframe['reponsablesPublics'])
    print("---------- ACTIVITES : ACTIONS : DECISIONS CONCERNEES ------------")
    print(final_flattened_dataframe['decisionsConcernees'])
    print("---------- ACTIVITES : ACTIONS : ACTIONS MENEES ------------")
    print(final_flattened_dataframe['actionsMenees'])
    print("---------- ACTIVITES : ACTIONS : TIERS ------------")
    print(final_flattened_dataframe['tiers'])
    return True
