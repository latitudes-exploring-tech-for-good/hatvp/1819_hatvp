Ce dossier contient des fichiers CSV dénormalisées (fusionnés) depuis `2_cleaned`. 

Leur objectif est de minimiser le nombre de fichiers, quitte à duppliquer beaucoup d'informations.

- Le fichier `vue_information_generales.csv` contient des informations essentiellement "statiques".
- Le fichier `vue_actions.csv` évolue avec les déclarations d'actions.