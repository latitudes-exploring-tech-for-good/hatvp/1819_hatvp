# -*- coding: utf-8 -*

import pandas as pd
import math
import datetime
import logging

logger = logging.getLogger(__name__)


def clean_dico_df(dico_df):
    '''
    Cette fonction nettoie les dataframes du dictionnaire :
    - renommer les noms des colonnes
    - re-classifier les données (notamment les champs "autre")
    - re-formater les dates et création de colonnes par année
    - re-classification dans "responsables_publics" des départements ministériels, etc
    - création de colonnes montant inf et sup des montants déclarés par exercice
    - création de colonnes chiffre d'affaires inf et sup des chiffres d'affaires déclarés par exercice
    '''

    logger.info(
        "Nettoyage des dataframes du dictionnaire (renommer les colonnes et re-classifier les données)(clean_dico_df)")

    dico_df = renaming_dataframes(dico_df)

    dico_df['informations_generales'] = cleaning_informations_generales(dico_df['informations_generales'])
    dico_df['dirigeants'] = cleaning_dirigeants(dico_df['dirigeants'])
    dico_df['collaborateurs'] = cleaning_collaborateurs(dico_df['collaborateurs'])
    dico_df['clients'] = cleaning_clients(dico_df['clients'])
    dico_df['affiliations'] = cleaning_affiliations(dico_df['affiliations'])
    dico_df['liste_secteurs_activites'] = cleaning_liste_secteurs_activites(dico_df['liste_secteurs_activites'])
    dico_df['liste_niveau_intervention'] = cleaning_liste_niveau_intervention(dico_df['liste_niveau_intervention'])
    dico_df['exercices'] = cleaning_exercices(dico_df['exercices'])
    dico_df['informations_generales_activites'] = cleaning_informations_generales_activites(
        dico_df['informations_generales_activites'])
    dico_df['domaines_intervention_actions_menees'] = cleaning_domaines_intervention_actions_menees(
        dico_df['domaines_intervention_actions_menees'])

    dico_df['beneficiaires_actions_menees'] = cleaning_beneficiaires_actions_menees(
        dico_df['beneficiaires_actions_menees'])
    dico_df['decisions_concernees'] = cleaning_decisions_concernees(dico_df['decisions_concernees'])

    dico_df = cleaning_responsables_publics_actions_menees(dico_df)
    dico_df = cleaning_actions_menees(dico_df)

    dico_df['actions_representation_interet'] = cleaning_actions_representation_interet(
        dico_df['actions_representation_interet'])

    dico_df = add_to_dico_conversion_table(dico_df)

    return dico_df


def renaming_dataframes(dico_df):
    new_df_names = {'affiliations': 'affiliations', 'clients': 'clients', 'collaborateurs': 'collaborateurs',
                    'dirigeants': 'dirigeants',
                    'publicationCourante_domainesIntervention': 'domaines_intervention_actions_menees',
                    'actionsMenees': 'actions_menees', 'decisionsConcernees': 'decisions_concernees',
                    'reponsablesPublics': 'responsables_publics_actions_menees',
                    'tiers': 'beneficiaires_actions_menees',
                    'publicationCourante_actionsRepresentationInteret': 'actions_representation_interet',
                    'publicationCourante_activites': 'informations_generales_activites', 'exercices': 'exercices',
                    'activites_listSecteursActivites': 'liste_secteurs_activites',
                    'activites_listNiveauIntervention': 'liste_niveau_intervention',
                    'representants': 'informations_generales'}

    return rename_df(dico_df, new_df_names)


def cleaning_informations_generales(df):
    df = drop_columns(df, ['nomUsage', 'publierMonAdressePhysique', 'ancienNomHatvp', 'publierMonAdresseEmail',
                           'publierMonTelephoneDeContact', 'dateCreation', 'categorieOrganisation_code',
                           'categorieOrganisation_categorie', 'emailDeContact', 'lienListeTiers', 'telephoneDeContact'])

    df = df.rename(index=str, columns={'sigleHatvp': 'sigle_HATVP', 'declarationTiers': 'declaration_tiers',
                                       'declarationOrgaAppartenance': 'declaration_organisation_appartenance',
                                       'isActivitesPubliees': 'activites_publiees', 'nomUsageHatvp': 'nom_usage_HATVP',
                                       'typeIdentifiantNational': 'type_identifiant_national',
                                       'identifiantNational': 'identifiant_national',
                                       'datePremierePublication': 'date_premiere_publication',
                                       'codePostal': 'code_postal', 'lienPageFacebook': 'page_facebook',
                                       'lienPageTwitter': 'page_twitter', "lienPageLinkedin": 'page_linkedin',
                                       'dateDernierePublicationActivite': 'derniere_publication_activite',
                                       'lienSiteWeb': 'site_web',
                                       'categorieOrganisation_label': 'label_categorie_organisation'})

    return df


def cleaning_dirigeants(df):
    df = df.rename(index=str, columns={'civilite': 'civilite_dirigeant', 'fonction': 'fonction_dirigeant',
                                       'nom': 'nom_dirigeant', 'prenom': 'prenom_dirigeant'})

    df['nom_prenom_dirigeant'] = df['nom_dirigeant'].str.cat(df['prenom_dirigeant'], sep=" ")

    return df


def cleaning_collaborateurs(df):

    df = df.rename(index=str, columns={'civilite': 'civilite_collaborateur', 'fonction': 'fonction_collaborateur',
                                       'nom': 'nom_collaborateur', 'prenom': 'prenom_collaborateur'})

    df['nom_prenom_collaborateur'] = df['nom_collaborateur'].str.cat(df['prenom_collaborateur'], sep=" ")

    return df


def cleaning_clients(df):
    df = df.rename(index=str, columns={'denomination': 'denomination_client',
                                       'identifiantNational': 'identifiant_national_client',
                                       'typeIdentifiantNational': 'type_identifiant_national_client'})
    return df


def cleaning_affiliations(df):
    df = df.rename(index=str, columns={'denomination': 'denomination_affiliation',
                                       'identifiantNational': 'identifiant_national_affiliation',
                                       'typeIdentifiantNational': 'type_identifiant_national_affiliation'})
    return df


def cleaning_liste_secteurs_activites(df):
    df = drop_columns(df, ['categorie', 'code', 'ordre'])
    df = df.rename(index=str, columns={'label': 'secteur_activite'})
    return df


def cleaning_liste_niveau_intervention(df):
    df = drop_columns(df, ['categorie', 'code', 'ordre'])
    df = df.rename(index=str, columns={'label': 'niveau_intervention'})
    return df


def cleaning_exercices(df):
    df = drop_columns(df, ['publicationCourante_exerciceId'])

    df = df.rename(index=str, columns={'publicationCourante_publicationDate': 'date_publication',
                                       'publicationCourante_dateDebut': 'date_debut',
                                       'publicationCourante_dateFin': 'date_fin',
                                       'publicationCourante_montantDepense': 'montant_depense',
                                       'publicationCourante_nombreSalaries': 'nombre_salaries',
                                       'publicationCourante_nombreActivite': 'nombre_activites',
                                       'publicationCourante_chiffreAffaire': 'chiffre_affaires',
                                       'publicationCourante_hasNotChiffreAffaire': 'exercice_sans_CA',
                                       'publicationCourante_noActivite': 'exercice_sans_activite',
                                       'publicationCourante_defautDeclaration': 'declaration_incomplete'})

    '''Transformer les dates en format date'''
    df.date_publication = df.date_publication.apply(clean_date)
    df.date_debut = df.date_debut.apply(clean_date)
    df.date_fin = df.date_fin.apply(clean_date)

    '''Ajouter les années de début et de fin d'exercices'''
    df['annee_debut'] = df['date_debut'].dt.year
    df['annee_fin'] = df['date_fin'].dt.year

    '''Ajouter les montants min et max à partir du montant dépensé par exercice'''
    df['montant_depense_inf'] = df['montant_depense'].map(
        lambda montant: montant_to_min_max(montant)[0])
    df['montant_depense_sup'] = df['montant_depense'].map(
        lambda montant: montant_to_min_max(montant)[1])

    '''Ajouter les chiffres d'affaires min et max à partir du chiffre d'affaire déclaré par exercice'''
    df['ca_inf'] = df['chiffre_affaires'].map(
        lambda CA: ca_to_min_max(CA)[0])
    df['ca_sup'] = df['chiffre_affaires'].map(
        lambda CA: ca_to_min_max(CA)[1])

    '''Transformer euros en €'''
    df['montant_depense'] = df['montant_depense'].replace("euros", "€", regex=True)
    df['chiffre_affaires'] = df['chiffre_affaires'].\
        replace("euros", "€", regex=True)

    return df


def cleaning_informations_generales_activites(df):
    df = df.rename(index=str, columns={'publicationCourante_activites_id': 'activite_id',
                                       'publicationCourante_publicationDate': 'date_publication_activite',
                                       'publicationCourante_objet': 'objet_activite',
                                       'publicationCourante_identifiantFiche': 'identifiant_fiche'})

    '''Transformer les dates'''
    df.date_publication_activite = df.date_publication_activite.apply(clean_date)

    return df


def cleaning_domaines_intervention_actions_menees(df):
    df = df.rename(index=str, columns={'publicationCourante_activites_id': 'activite_id',
                                       0: 'domaines_intervention_actions_menees'})

    return df


def cleaning_beneficiaires_actions_menees(df):
    df = df.rename(index=str,
                   columns={'publicationCourante_actionsRepresentationInteret_id': 'action_representation_interet_id',
                            0: 'beneficiaire_action_menee'})

    df["action_menee_en_propre"] = 0
    mask_en_propre = df.beneficiaire_action_menee.str.contains("en propre")
    df.action_menee_en_propre[mask_en_propre] = 1
    df.beneficiaire_action_menee[mask_en_propre] = df.beneficiaire_action_menee[
        mask_en_propre].str.replace(" \(en propre\)", "")

    return df


def cleaning_decisions_concernees(df):
    df = df.rename(index=str,
                   columns={'publicationCourante_actionsRepresentationInteret_id': 'action_representation_interet_id',
                            0: 'decision_concernee'})

    return df


def cleaning_actions_menees(dico_df):
    dico_df['actions_menees'] = reclassify_label(dico_df['actions_menees'],
                                                 dico_df['actions_representation_interet'], 'actionMeneeAutre',
                                                 'publicationCourante_actionsRepresentationInteret_id')

    dico_df['actions_menees'] = dico_df['actions_menees'].rename(
        index=str, columns={'publicationCourante_actionsRepresentationInteret_id': 'action_representation_interet_id',
                            0: 'action_menee', 'actionMeneeAutre': 'action_menee_autre'})

    return dico_df


def cleaning_responsables_publics_actions_menees(dico_df):
    dico_df['responsables_publics_actions_menees'] = reclassify_label(
        dico_df['responsables_publics_actions_menees'], dico_df['actions_representation_interet'],
        'responsablePublicAutre', 'publicationCourante_actionsRepresentationInteret_id')

    dico_df['responsables_publics_actions_menees'] = \
        dico_df['responsables_publics_actions_menees'].rename(
            index=str,
            columns={0: 'responsable_public',
                     'publicationCourante_actionsRepresentationInteret_id': 'action_representation_interet_id',
                     'responsablePublicAutre': 'responsable_public_ou_dpt_ministeriel_autre'})

    dico_df['responsables_publics_actions_menees'] = flattened_responsables_publics(
        dico_df['responsables_publics_actions_menees'])

    return dico_df


def cleaning_actions_representation_interet(df):
    df = drop_columns(df, ['actionMeneeAutre', 'responsablePublicAutre'])
    df = df.rename(index=str, columns={'publicationCourante_activites_id': 'activite_id',
                                       'publicationCourante_actionsRepresentationInteret_id':
                                           'action_representation_interet_id'})

    return df


def add_to_dico_conversion_table(dico_df):
    dico_df['conversion_table'] = drop_columns(
        dico_df['informations_generales'], ['adresse', 'pays', 'ville', 'code_postal',
                                                    'derniere_publication_activite',
                                                    'site_web',
                                                    'label_categorie_organisation'])
    return dico_df


def reclassify_label(df_fille, df_mere, label, key_df_fille):
    df = df_mere
    list_columns_to_conserve = [label, key_df_fille]
    for column in df_mere.columns:
        if not column in list_columns_to_conserve:
            df = df.drop(columns=column)

    df = df_fille.merge(df, on=key_df_fille)
    return df


def drop_columns(df, L):
    for e in L:
        df = df.drop(columns=e)
    return df


def rename_df(dico_df, new_name):
    return {new_name[name]: df for name, df in dico_df.items()}


def clean_date(date):
    try:
        return datetime.datetime.strptime(date[0:10], '%d/%m/%Y')
    except:
        pass

    try:
        return datetime.datetime.strptime(date[0:10], '%d-%m-%Y')
    except:
        pass


def ca_to_min_max(fourchette_ca):
    # Cette fonction permet de récupérer le min et max de la fourchette du chiffre d'affaires déclaré par exercice
    if type(fourchette_ca) == float:
        CA_inf = 0
        CA_sup = 0
    else:
        if fourchette_ca.find("> =") >= 0:
            inf_start = fourchette_ca.find("> =") + 4
            inf_end = fourchette_ca.find(" euros")
            CA_inf = int(fourchette_ca[inf_start:inf_end].replace(" ", ""))
        else:
            CA_inf = 0
        if fourchette_ca.find("<") >= 0:
            sup_start = fourchette_ca.find("<") + 2
            sup_end = -6
            CA_sup = int(fourchette_ca[sup_start:sup_end].replace(" ", ""))
        else:
            CA_sup = math.inf
    return CA_inf, CA_sup


def montant_to_min_max(fourchette_montant):
    # Cette fonction permet de récupérer le min et max de la fourchette du montant dépensé par exercice
    if type(fourchette_montant) == float:
        montant_inf = 0
        montant_sup = 0
    else:
        if fourchette_montant.find("> =") >= 0:
            inf_start = fourchette_montant.find("> =") + 4
            inf_end = fourchette_montant.find(" euros")
            montant_inf = int(fourchette_montant[inf_start:inf_end].replace(" ", ""))
        else:
            montant_inf = 0
        if fourchette_montant.find("<") >= 0:
            sup_start = fourchette_montant.find("<") + 2
            sup_end = -6
            montant_sup = int(fourchette_montant[sup_start:sup_end].replace(" ", ""))
        else:
            montant_sup = math.inf
    return montant_inf, montant_sup


MEMBRE_GOUVERNEMENT = "Membre du Gouvernement ou membre de cabinet ministériel"
DIRECTEUR_SECRETAIRE = "Directeur ou secrétaire général, ou leur adjoint, ou membre du collège ou d'une commission des sanctions d'une autorité administrative ou publique indépendante"


def flattened_responsables_publics(responsables_publics_df):
    # Cette fonction permet de reclassifier les cabinets et départements dans le cas où
    # les responsables publics sollicités sont des membres du gouvernement ou d'une autorité administrative
    new_df_resp_public = pd.DataFrame(
        columns=['action_representation_interet_id',
                 'responsable_public', 'departement_ministeriel', 'responsable_public_ou_dpt_ministeriel_autre'])

    for i in responsables_publics_df.index:
        denomination_resp_public = responsables_publics_df['responsable_public'].loc[i]
        action_id = responsables_publics_df['action_representation_interet_id'].loc[i]
        resp_autre = responsables_publics_df['responsable_public_ou_dpt_ministeriel_autre'].loc[i]

        if denomination_resp_public.find(MEMBRE_GOUVERNEMENT) is not -1 or denomination_resp_public.find(
                DIRECTEUR_SECRETAIRE) is not -1:
            start = denomination_resp_public.find("-") + 2
            list_departement = denomination_resp_public[start:].split(",")

            for el in list_departement:
                resp = denomination_resp_public[:start - 3]
                departement = el

                new_df_resp_public.loc[len(new_df_resp_public)] = [action_id, resp, departement, resp_autre]
            else:
                new_df_resp_public.loc[len(new_df_resp_public)] = [action_id, denomination_resp_public, "", resp_autre]

    return new_df_resp_public.fillna("")



