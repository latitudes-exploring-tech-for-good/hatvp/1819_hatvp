#!/usr/bin/env python
# coding: utf-8

import os
import json
import pandas as pd
import requests
import logging

try:
    import configparser as ConfigParser
except ImportError:
    import ConfigParser

config = ConfigParser.RawConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '..', 'agora_explorer.config'))

logger = logging.getLogger(__name__)

REPERTOIRE_URL = config['REPERTOIRE']['Url']
START_NAME = 'representants'
DATA_DIR = os.path.join(os.path.dirname(__file__), '..', 'data')
RAW_DATA_DIR = os.path.join(DATA_DIR, "0_raw")
JSON_FILENAME = "agora_repertoire_opendata"
REPERTOIRE_JSON_PATH = os.path.join(RAW_DATA_DIR, JSON_FILENAME+".json")
READABLE_REPERTOIRE_JSON_PATH = os.path.join(RAW_DATA_DIR, JSON_FILENAME+"_readable.json")
FLATTENED_TABLES_DIR = os.path.join(DATA_DIR, "1_flattened")
JOIN_DIRECTORY = os.path.join(DATA_DIR, "joined")


def download_raw_json():
    '''
    Cette méthode télécharge les données du répertoire au format JSON à partir du site de la HATVP
    '''
    logger.info("Téléchargement des données du répertoire au format JSON depuis le site de la HATVP (download_raw_json)")
    request = requests.get(REPERTOIRE_URL)
    try:
        request.raise_for_status()
    except requests.exceptions.HTTPError as err:
        logger.error('Erreur dans le téléchargement des données du répertoire : %s', err)
        exit(0)
    return request

def write_json_to_folder(request):
    '''
    Enregistre le json dans 0_raw
    '''
    repertoire = request.json()
    try:
        with open(REPERTOIRE_JSON_PATH, 'wb') as f:
            f.write(request.content)
        with open(READABLE_REPERTOIRE_JSON_PATH, 'w', encoding='utf8') as f:
            json.dump(repertoire, f, indent=4)
    except EnvironmentError as err:
        logger.error('Erreur dans l\'ecriture du fichier JSON en local : %s', err)
        exit(0)
    return True


def extract_json_to_dataframe(dataframe):
    '''
    Cette méthode part du fichier JSON et le met à plat dans un dictionnaire de dataframes
    '''
    logger.info("Mise à plat du fichier JSON dans un dictionnaire de dataframes (extract_json_to_dataframe)")
    df_repertoire = dataframe
    df = df_all_flattened(df_repertoire)
    dict_dataframe = dict()
    process_df_dataframe(df, START_NAME, dict_dataframe)
    return dict_dataframe

def read_repertoire_as_dataframe():
    repertoire = ""
    try:
        with open(REPERTOIRE_JSON_PATH, "r", encoding='utf-8') as f:
            repertoire = json.load(f)
    except EnvironmentError as err:
        logger.error('Erreur dans la lecture du fichier opendata : %s', err)
        exit(0)
    return pd.DataFrame(repertoire['publications'])

def df_all_flattened(df):
    dict_column = get_dict_columns(df)
    while dict_column != []:
        for d in dict_column:
            df = flatten_dict_column(df, d)
        dict_column = get_dict_columns(df)
    return df


def process_df_dataframe(df, df_name, dict_dataframe):
    '''
    Cette méthode met à plat chaque dataframe (tant qu'il reste des listes dans les colonnes) et enregistre l'instance dans un dictionnaire
    '''
    list_column = get_list_columns(df)

    if get_list_columns(df) == []:
        save_dataframe(dict_dataframe, df, df_name)
        pass
    else:
        df[df_name + '_id'] = list(df.index)
        cols = [df_name + '_id'] + [col for col in df if col != df_name + '_id']
        df = df[cols].copy(deep=True)
        for l in list_column:
            sub_df = df_all_flattened(extract_related_table_from_list_column(df, l, df_name))
            process_df_dataframe(sub_df, l, dict_dataframe)
        save_dataframe(dict_dataframe, df, df_name)


def get_dict_columns(df):
    return list(df.columns[df.iloc[0, :].map(type) == dict])


def flatten_dict_column(df, dict_column):
    '''
    A partir d'un df et d'une colonne du df qui contient des dictionnaires,
    la méthode renvoie ce même df mais pour lequel le contenu des dict est remonté
    '''
    sub_df = df[dict_column].apply(pd.Series)
    sub_df.columns = [dict_column + '_' + sub_column for sub_column in sub_df.columns]
    df = df.drop(labels=dict_column, axis='columns')
    df = pd.concat([df, sub_df], axis=1, sort=False)
    return df


def get_list_columns(df):
    L = []
    for j in range(0, len(df.columns)):
        for i in range(0, len(df)):
            if type(df[df.columns[j]][i]) == list:
                L.append(df.columns[j])
                break
    return L


def extract_related_table_from_list_column(df, list_column, df_name):
    '''
    A partir d'un df, de l'intitulé de la colonne qui contient des listes et du nom de la table liée,
    la méthode retourne la table liée et supprime du df initial la colonne concernée
    '''
    new_index = list()
    for i in df.index:
        if type(df[list_column][i]) == list:
            new_index.append(i)

    list_of_flattened_df = list(df[list_column].dropna(how='all').map(pd.DataFrame))
    df.drop(labels=list_column, axis='columns', inplace=True)
    list_of_flattened_df_with_index = list()
    for i, flattened_df in zip(new_index, list_of_flattened_df):
        flattened_df[df_name + '_id'] = i
        list_of_flattened_df_with_index.append(flattened_df)
    related_table = pd.concat(list_of_flattened_df_with_index, sort=False)
    related_table.index = [i for i in range(0, len(related_table))]
    return related_table


def export_table_csv(df, directory, table_name):
    df.to_csv(os.path.join(directory, table_name + '.csv'), index=False, sep=";")


def export_table_excel(df, directory, table_name):
    df.to_excel(os.path.join(directory, table_name + ".xlsx"), index=False)


def save_dataframe(dict_dataframe, dataframe_to_save, dataframe_key):
    dict_dataframe[dataframe_key] = dataframe_to_save


def extract_json_to_tables(dataframe):
    # Extraction des tables flattened au format .CSV
    df_repertoire = dataframe
    df = df_all_flattened(df_repertoire)
    process_df_csv(df, START_NAME)


def process_df_csv(df, df_name):
    # Mettre à plat le JSON au format .CSV
    list_column = get_list_columns(df)

    if get_list_columns(df) == []:
        export_table_csv(df, FLATTENED_TABLES_DIR, df_name)
        pass
    else:
        df[df_name + '_id'] = list(df.index)
        cols = [df_name + '_id'] + [col for col in df if col != df_name + '_id']
        df = df[cols]
        for l in list_column:
            sub_df = df_all_flattened(extract_related_table_from_list_column(df, l, df_name))
            process_df_csv(sub_df, l)
        export_table_csv(df, FLATTENED_TABLES_DIR, df_name)
