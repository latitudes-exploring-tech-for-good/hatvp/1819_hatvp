import logging
import os
import pandas as pd

from util.utils import create_postgres_engine, execute_text_query
from util.extract_json import export_table_csv, export_table_excel
from util.cleaning import drop_columns
from model.uploader import Uploader
from util.create_documentation import *

# DATA_DIR = os.path.join(os.path.dirname(__file__), 'data')
VUE_INFORMATIONS_GENERALES = 'vue_informations_generales'
  VUE_ACTIONS = 'vue_actions'
DATA_DIR = os.path.join(os.path.dirname(__file__), '..', 'data')
VIEWS = os.path.join(DATA_DIR, "4_views")
CLEANED = os.path.join(DATA_DIR, "2_cleaned")
CLEANED_XLS = os.path.join(DATA_DIR, "2_cleaned_xls")
MERGED = os.path.join(DATA_DIR, "3_merged")

logger = logging.getLogger(__name__)

INDEX_TEMPLATE = '''
CREATE INDEX IF NOT EXISTS "{table}_index_on_{index_column}" 
ON public."{table}" ("{index_column}");
'''
PREFIX_INDEX_TEMPLATE = '''
CREATE INDEX IF NOT EXISTS "{table}_prefix_index_on_{index_column}" 
ON public."{table}" (lower("{index_column}") text_pattern_ops, "{index_column}");
'''


def create_cleaned_tables(dico_df):
    '''
    Génération des tables granulaires en csv
    '''
    logging.info("Génération de tables granulaires au format CSV dans 2_cleaned (create_cleaned_tables)")
    export_table_csv(dico_df['informations_generales'], CLEANED, 'informations_generales')
    export_table_csv(dico_df['dirigeants'], CLEANED, 'dirigeants')
    export_table_csv(dico_df['collaborateurs'], CLEANED, 'collaborateurs')
    export_table_csv(dico_df['clients'], CLEANED, 'clients')
    export_table_csv(dico_df['affiliations'], CLEANED, 'affiliations')
    export_table_csv(dico_df['liste_secteurs_activites'], CLEANED, 'liste_secteurs_activites')
    export_table_csv(dico_df['liste_niveau_intervention'], CLEANED, 'liste_niveau_intervention')
    export_table_csv(dico_df['exercices'], CLEANED, 'exercices')
    export_table_csv(dico_df['informations_generales_activites'], CLEANED, 'informations_generales_activites')
    export_table_csv(dico_df['actions_representation_interet'], CLEANED, 'actions_representation_interet')
    export_table_csv(dico_df['domaines_intervention_actions_menees'], CLEANED, 'domaines_intervention_actions_menees')
    export_table_csv(dico_df['actions_menees'], CLEANED, 'actions_menees')
    export_table_csv(dico_df['responsables_publics_actions_menees'], CLEANED, 'responsables_publics_actions_menees')
    export_table_csv(dico_df['beneficiaires_actions_menees'], CLEANED, 'beneficiaires_actions_menees')
    export_table_csv(dico_df['decisions_concernees'], CLEANED, 'decisions_concernees')

    export_table_excel(dico_df['informations_generales'], CLEANED_XLS, 'informations_generales')
    export_table_excel(dico_df['dirigeants'], CLEANED_XLS, 'dirigeants')
    export_table_excel(dico_df['collaborateurs'], CLEANED_XLS, 'collaborateurs')
    export_table_excel(dico_df['clients'], CLEANED_XLS, 'clients')
    export_table_excel(dico_df['affiliations'], CLEANED_XLS, 'affiliations')
    export_table_excel(dico_df['liste_secteurs_activites'], CLEANED_XLS, 'liste_secteurs_activites')
    export_table_excel(dico_df['liste_niveau_intervention'], CLEANED_XLS, 'liste_niveau_intervention')
    export_table_excel(dico_df['exercices'], CLEANED_XLS, 'exercices')
    export_table_excel(dico_df['informations_generales_activites'], CLEANED_XLS, 'informations_generales_activites')
    export_table_excel(dico_df['actions_representation_interet'], CLEANED_XLS, 'actions_representation_interet')
    export_table_excel(dico_df['domaines_intervention_actions_menees'], CLEANED_XLS, 'domaines_intervention_actions_menees')
    export_table_excel(dico_df['actions_menees'], CLEANED_XLS, 'actions_menees')
    export_table_excel(dico_df['responsables_publics_actions_menees'], CLEANED_XLS, 'responsables_publics_actions_menees')
    export_table_excel(dico_df['beneficiaires_actions_menees'], CLEANED_XLS, 'beneficiaires_actions_menees')
    export_table_excel(dico_df['decisions_concernees'], CLEANED_XLS, 'decisions_concernees')

    export_table_excel(generate_documentation_tables(dico_df), CLEANED, 'documentation_vues_separees')
    export_table_excel(generate_documentation_tables(dico_df), CLEANED_XLS, 'documentation_vues_separees')

def create_merged_tables(dico_df):
    '''
    Génération des tables intermédiaires fusionnées
    '''

    '''
    informations_generales_activites
    - actions_representation_interet
     - decisions_concernees
     - beneficiaires_actions_menees
     - responsables_publics_actions_menees
     - actions_menees
     - domaines_intervention_actions_menees
    '''
    logging.info("Génération de tables intermédiaires fusionnées au format CSV dans 3_merged (create_merged_tables)")
    merge_activites_1 = dico_df['informations_generales_activites'].merge(
        dico_df['actions_representation_interet'], on='activite_id', how='outer').merge(
        dico_df['decisions_concernees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['beneficiaires_actions_menees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['responsables_publics_actions_menees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['actions_menees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['domaines_intervention_actions_menees'], on='activite_id', how='outer')

    '''
    On propose également une vue merged où sont ajoutées les colonnes par type d'actions menées, par type de decision 
    concernée, par responsable public et par domaine d'intervention
    '''
    merge_activites_2 = to_dummies(merge_activites_1, ['decision_concernee', 'responsable_public', 'action_menee',
                                                       'domaines_intervention_actions_menees'])

    '''
    exercices
    '''
    merge_exercices = dico_df['exercices']

    '''
    dirigeants
    '''
    merge_dirigeants = drop_columns(dico_df['dirigeants'], ['nom_prenom_dirigeant'])
    '''
    collaborateurs
    '''
    merge_collaborateurs = drop_columns(dico_df['collaborateurs'], ['nom_prenom_collaborateur'])

    '''
    informations_generales
    liste_secteurs_activites
    liste_niveau_intervention
    '''
    merge_intervention = dico_df['liste_secteurs_activites'].merge(
        dico_df['liste_niveau_intervention'], on='representants_id', how='outer')

    merge_informations_generales = dico_df['informations_generales'].merge(
        merge_intervention, on='representants_id', how='outer')

    merge_informations_generales = to_dummies(merge_informations_generales,
                                              ['secteur_activite', 'niveau_intervention'])

    '''
        affiliations
        '''
    merge_affiliations = dico_df['affiliations']
    merge_affiliations['affiliation_est_declarante'] = \
        merge_affiliations['denomination_affiliation'].isin(merge_informations_generales['denomination'])

    '''
    clients
    '''
    merge_clients = dico_df['clients']
    merge_clients['client_est_declarant'] = \
        merge_clients['denomination_client'].isin(merge_informations_generales['denomination'])

    '''
    Export des 3_merged
    '''
    export_table_csv(merge_activites_1, MERGED, 'merge_activites_1')
    export_table_csv(merge_activites_2, MERGED, 'merge_activites_2')
    export_table_csv(merge_dirigeants, MERGED, 'merge_dirigeants')
    export_table_csv(merge_affiliations, MERGED, 'merge_affiliations')
    export_table_csv(merge_clients, MERGED, 'merge_clients')
    export_table_csv(merge_collaborateurs, MERGED, 'merge_collaborateurs')
    export_table_csv(merge_exercices, MERGED, 'merge_exercices')
    export_table_csv(merge_informations_generales, MERGED, 'merge_informations_generales')

    dico_df_merged = {'merge_activites_1': merge_activites_1,
                      'merge_activites_2': merge_activites_2,
                      'merge_dirigeants': merge_dirigeants,
                      'merge_affiliations': merge_affiliations,
                      'merge_clients': merge_clients,
                      'merge_collaborateurs': merge_collaborateurs,
                      'merge_exercices': merge_exercices,
                      'merge_informations_generales': merge_informations_generales}
    export_table_excel(generate_documentation_tables(dico_df_merged), MERGED, 'documentation_vues_intermediaires')


def to_dummies(df, list_columns_to_dummies):
    return pd.get_dummies(df, columns=list_columns_to_dummies, prefix_sep=' : ')


def create_views_tables(dico_df):
    '''
    Génération des deux tables à charger dans postgres
    '''
    logging.info(
        "Génération des deux tables agrégées au format CSV dans 4_views à charger dans postgres (create_views_tables)")
    vue_actions = dico_df['conversion_table'].merge(dico_df['exercices'], on='representants_id').merge(
        dico_df['informations_generales_activites'], on='exercices_id', how='outer').merge(
        dico_df['domaines_intervention_actions_menees'], on='activite_id', how='outer').merge(
        dico_df['actions_representation_interet'], on='activite_id', how='outer').merge(
        dico_df['responsables_publics_actions_menees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['beneficiaires_actions_menees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['actions_menees'], on='action_representation_interet_id', how='outer').merge(
        dico_df['decisions_concernees'], on='action_representation_interet_id', how='outer')

    vue_informations_generales = dico_df['informations_generales'].merge(
        dico_df['clients'], on='representants_id', how='outer').merge(
        dico_df['dirigeants'], on='representants_id', how='outer').merge(
        dico_df['collaborateurs'], on='representants_id', how='outer')

    export_table_csv(vue_actions, VIEWS, VUE_ACTIONS)
    export_table_csv(vue_informations_generales, VIEWS, VUE_INFORMATIONS_GENERALES)

    dico_df_views = {'vue_actions': vue_actions, 'vue_informations_generales': vue_informations_generales}
    export_table_excel(generate_documentation_tables(dico_df_views), VIEWS, 'documentation_vues_fusionnées')


def create_psql_tables():
    '''
    Génération des tables à charger dans postgres
    '''
    for file in os.listdir(VIEWS):
        if not file.endswith('.csv'):
            continue
        logger.info(file)
        uploader = Uploader(csv_directory=VIEWS,
                            csv_name=file,
                            separator=';')
        uploader.reset()
        uploader.run()

def add_indexes():
    engine = create_postgres_engine()
    for (table, column, create_prefix_index) in [
        (VUE_ACTIONS, "beneficiaire_action_menee", True),
        (VUE_ACTIONS, "denomination", True),
        (VUE_ACTIONS, "annee_fin", False),

    ]:
        index_query = INDEX_TEMPLATE.format(table=table, index_column=column)
        execute_text_query(engine, index_query)
        if create_prefix_index:
            index_query = PREFIX_INDEX_TEMPLATE.format(table=table, index_column=column)
            execute_text_query(engine, index_query)
