import os
import numpy as np
import pandas as pd
from copy import copy, deepcopy


def clean_list_files(list_files):
    return_list = []
    for name in list_files:
        if ".csv" in name:
            return_list += [name]

    return return_list


def traitement_majuscules(chaine):
    majuscules_index = []
    for k in range(len(chaine)):
        if chaine[k].isupper():
            majuscules_index += [k]

    if majuscules_index != []:
        k = 0
        for i in majuscules_index:
            if i != 0:
                if chaine[i - 1 + k] != "_":
                    chaine = chaine[:i + k] + '_' + chaine[i + k:]
                    k += 1

    return chaine.lower()


def traitement_hatvp(field_name):
    if "hatvp" in field_name:
        field_name = field_name.replace("hatvp", "HATVP")

    return field_name


def creation_id_description(field_name):
    formated_name = field_name[:-3].replace('_', ' ').title()
    if formated_name == "Representants":
        formated_name = "représentant d'intérêts"
    if formated_name == "Activite":
        formated_name = "activité"
    if formated_name == "Action Representation Interet":
        formated_name = "action de représentation d'intérêts"
    if formated_name == "Exercices":
        formated_name = "exercice"

    description = "Identifiant unique pour chaque {}".format(formated_name)
    return description


def traitement_id_fields(df):
    df1 = deepcopy(df)
    mask_id = df1.field_name.str.endswith('_id')
    df1.description[mask_id] = df1[mask_id].field_name.map(creation_id_description)

    return df1


def clean_list_noms_tables(list_tables):
    for name in list_tables:
        if ".csv" not in name:
            list_tables.remove(name)

    return list_tables


INFOS_HATVP = pd.read_excel(
    os.path.join(os.path.dirname(__file__), '..', 'descriptif_JSON_AGORA.xlsx'))
INFOS_HATVP.loc[0]['INTITULE'] = INFOS_HATVP.loc[0]['INTITULE'].lower()

INFOS_HATVP.INTITULE = INFOS_HATVP.INTITULE.apply(traitement_majuscules)
INFOS_HATVP.INTITULE = INFOS_HATVP.INTITULE.apply(traitement_hatvp)

INFOS_FIELDS_IN_JSON = {'beneficiaire_action_menee': "Tiers pour lequel l'organisation a "
                                                     "effectué l'activité déclarée",
                        'denomination_client': "Raison sociale du client ou mandant",
                        'identifiant_national_client': "Identifiant du client ou mandant",
                        'type_identifiant_national_client': "Identifiant utilisé par l'organisation : numéro SIREN ou RNA (ou à défaut numéro attribué par la HATVP)",
                        'responsable_public_autre': "Champ à remplir dans le cas où la catégorie du responsable public "
                                                    "concerné par l'action de représentation d'intérêts n'est pas "
                                                    "présente dans la liste proposée par défaut par la HATVP",
                        'responsable_public': "Catégories de responsables publics avec lesquels l'organisation est entrée en communication au cours de l'activité déclarée",
                        'departement_ministeriel': "Si précisé, le ministère auquel est rattaché le responsable public "
                                                   "auprès duquel l'organisation a mené une action de "
                                                   "représentation d'intérêts",
                        'responsable_public_ou_dpt_ministeriel_autre': "Si ministère ou responsable public absent de "
                                                                       "la liste par défaut proposée par la HATVP, "
                                                                       "champ à compléter par le représentant "
                                                                       "d'intérêts",
                        'civilite_dirigeant': "Civilité du dirigeant",
                        'fonction_dirigeant': "Fonction du dirigeant",
                        'nom_dirigeant': "Nom du dirigeant",
                        'prenom_dirigeant': "Prénom du dirigeant",
                        'nom_prenom_dirigeant': "Nom et prénom du dirigeant",
                        'date_publication_activite': "Date de publication de l'activité",
                        'niveau_intervention': "Niveaux d’intervention auxquels l'organisation conduit ses actions de "
                                               "représentation d’intérêts",
                        'declaration_incomplete': "L'organisation apparaît, ou non, dans la liste des représentants "
                                                  "d'intérêts n'ayant pas communiqué à la Haute Autorité tout ou "
                                                  "partie des informations exigibles par la loi",
                        'civilite_collaborateur': "Civilité du collaborateur",
                        'fonction_collaborateur': "Fonction du collaborateur",
                        'nom_collaborateur': "Nom du collaborateur",
                        'prenom_collaborateur': "Prénom du collaborateur",
                        'nom_prenom_collaborateur': "Nom et prénom du collaborateur",
                        'denomination_affiliation': "Raison sociale de l'organisation professionnelle ou "
                                                    "syndicale ou association",
                        'identifiant_national_affiliation': "Identifiant de l'organisation professionnelle "
                                                            "ou syndicale ou association",
                        'type_identifiant_national_affiliation': "Type de l'identifiant : numéro SIREN ou RNA (ou à défaut numéro attribué par la HATVP)",
                        'derniere_publication_activite': "Dernière publication (celle affichée sur le site) de "
                                                         "l'activité concernée",
                        'activites_publiees': "L'organisation déclare avoir mené ou non des activités pour l'exercice concerné",
                        'label_categorie_organisation': "Intitulé de la catégorie correspondant à l'organisation",
                        'action_menee': "Le ou les types d’actions mises en oeuvre dans le cadre de l’activité de "
                                        "représentation d’intérêts déclarée",
                        'action_menee_autre': "Champ à remplir dans le cas ou le type d'action mené dans le cadre de l'activité de représentation d'intérêts n'est pas présent dans la liste proposée par défaut par la HATVP",
                        "secteur_activite": "Liste des secteurs dans lesquels l'organisation réalise ses actions de représentation d’intérêts",
                        "decision_concernee": "Le ou les types de décisions publiques sur lesquelles a porté "
                                              "l’activité de représentation d’intérêts déclarée",
                        "objet_activite": "Question sur laquelle a porté l’activité de représentation d’intérêts "
                                          "déclarée",
                        "domaines_intervention_actions_menees": "Domaines d'intervention sur lesquels ont porté "
                                                                "les actions de représentation d'intérêts",
                        "exercice_sans_activite": "L'organisation déclare ne pas avoir eu d'activités pour l'exercice concerné",
                        "nombre_activites": "Nombre d'activités publiées pour l'exercice concerné",
                        "date_publication": "Date de publication de l'activité",
                        "exercice_sans_CA": "L'organisation déclare ne pas avoir de chiffre d'affaires pour l'exercice sélectionné",
                        "chiffre_affaires": "Chiffre d'affaires réalisé par l'organisation pour l'exercice sélectionné",
                        "declaration_organisation_appartenance": "L'organisation déclare, ou non, les organisations professionnelles ou syndicales ou les associations en lien avec les intérêts représentés auxquelles elle appartient",
                        "page_facebook": "Lien vers la page Facebook de l'organisation",
                        "page_linkedin": "Lien vers la page Linkedin de l'organisation",
                        "page_twitter": "Lien vers la page Twitter de l'organisation",
                        "site_web": "Site Internet de l'organisation"}

INFOS_FIELDS_NOT_IN_JSON = {'annee_debut': "Date de début de l'exercice concerné",
                            'annee_fin': "Date de clôture de l'exercice concerné",
                            'montant_depense_inf': "Borne inférieure du montant des dépenses liées aux activités de représentation d’intérêts réalisées pendant l’exercice concerné",
                            'montant_depense_sup': "Borne supérieure du montant des dépenses liées aux activités de représentation d’intérêts réalisées pendant l’exercice concerné",
                            'ca_inf': "Borne inférieure du chiffre d'affaires réalisé par l'organisation "
                                      "pour l'exercice sélectionné",
                            'ca_sup': "Borne supérieure du chiffre d'affaires réalisé par l'organisation "
                                      "pour l'exercice sélectionné",
                            'action_menee_en_propre': "Si 1, le bénéficiaire de l'action de lobbying a réalisé lui-même l'action. si 0, l'action a été réalisée par un tiers"}


def generate_documentation_tables(dico_of_df):
    df_documentation = pd.DataFrame(columns=['field_name', 'description', 'table'])

    for table in list(dico_of_df.keys()):
        for field in dico_of_df[table].columns:
            df_documentation.loc[len(df_documentation)] = [field, '', table]

    df_documentation = df_documentation.replace("", np.nan)

    # Ajout des descriptions des champs relatifs aux différents id
    df_documentation = traitement_id_fields(df_documentation)

    # On complète df_documentation avec des informations venant d'autres sources de renseignements

    # Informations venant directement du document de description de la HATVP
    for index in list(df_documentation[df_documentation.description.isna()].index):
        try:
            df_documentation.loc[index].description = INFOS_HATVP[
                INFOS_HATVP.INTITULE == df_documentation.loc[index].field_name].DESCRIPTION.values[
                0].capitalize()
        except:
            pass

    # Informations supplémentaires concernant les champs non décris et présents dans le JSON
    for index in list(df_documentation[df_documentation.description.isna()].index):
        try:
            df_documentation.loc[index].description = INFOS_FIELDS_IN_JSON[
                df_documentation.loc[index].field_name].capitalize()
        except:
            pass

    # Informations supplémentaires concernant les champs non décris mais ajoutés par rapport au JSON
    for index in list(df_documentation[df_documentation.description.isna()].index):
        try:
            df_documentation.loc[index].description = INFOS_FIELDS_NOT_IN_JSON[
                df_documentation.loc[index].field_name].capitalize()
        except:
            pass

    return df_documentation
