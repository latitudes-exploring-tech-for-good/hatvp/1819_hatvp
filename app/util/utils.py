import logging
import os

import pandas as pd
from sqlalchemy import create_engine, text

try:
    import configparser as ConfigParser
except ImportError:
    import ConfigParser

config = ConfigParser.RawConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '..', 'agora_explorer.config'))

logger = logging.getLogger(__name__)


def create_postgres_engine(user=config['POSTGRES']['User'], password=config['POSTGRES']['Password'],
                           host=config['POSTGRES']['Host'], port=config['POSTGRES']['Port'],
                           database=config['POSTGRES']['Database']):
    postgres_url = "postgresql://{user}:{password}@{host}:{port}/{database}" \
        .format(user=user, password=password, host=host, port=port, database=database)
    return create_engine(postgres_url)


def execute_text_query(engine, raw_query):
    query = text(raw_query)
    logger.info("Execute query : {}".format(query))
    result = engine.execute(query)
    if result.returns_rows:
        for row in result:
            logger.info(row)


def quoted(column_name):
    return '"{}"'.format(column_name)


def comma_separated_columns(columns):
    return ', '.join([quoted(column_name) for column_name in columns])


def get_csv_columns(csv_path, separator):
    if os.path.exists(csv_path):
        return list(pd.read_csv(csv_path, sep=separator, nrows=1).columns)
    else:
        raise FileNotFoundError("CSV file '{}' does not exists".format(csv_path))
