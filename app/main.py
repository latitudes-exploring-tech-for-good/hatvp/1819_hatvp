import json
import logging.config
import os
from util.cleaning import clean_dico_df
from util.create_tables import create_cleaned_tables, create_merged_tables, create_views_tables, create_psql_tables, add_indexes
from util.extract_json import download_raw_json, write_json_to_folder, extract_json_to_dataframe, read_repertoire_as_dataframe


def setup_logging(
        default_path='log_config.json',
        default_level=logging.INFO,
        env_key='LOG_CFG'
):
    """
    Setup logging configuration
    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


def main():
    setup_logging()
    logger = logging.getLogger(__name__)
    logger.info('Lancement AGORA EXPLORER')

    request = download_raw_json()
    write_json_to_folder(request)
    dataframe = read_repertoire_as_dataframe()
    dico_df = extract_json_to_dataframe(dataframe)
    dico_df = clean_dico_df(dico_df)
    create_cleaned_tables(dico_df)
    create_merged_tables(dico_df)
    create_views_tables(dico_df)

    create_psql_tables()
    add_indexes()

    logger.info('Fin execution AGORA EXPLORER')

if __name__ == '__main__':
    main()
