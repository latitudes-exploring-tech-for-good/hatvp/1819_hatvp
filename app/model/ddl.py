from util.utils import quoted


class TableDDL:
    CREATE_TEMPLATE = """
CREATE TABLE "{name}" (
{columns_lines}
);
    """
    DROP_TEMPLATE = """DROP TABLE IF EXISTS "{name}" CASCADE;\n"""
    COLUMN_LINE_TEMPLATE = "    {quoted_column_name} {column_type}"

    def __init__(self, name, columns, columns_types_other_than_text):
        self.name = name
        self.columns = columns
        self.columns_types_other_than_text = columns_types_other_than_text

    @property
    def drop_ddl(self):
        return self.DROP_TEMPLATE.format(name=self.name)

    @property
    def create_ddl(self):
        return self.CREATE_TEMPLATE.format(name=self.name, columns_lines=self._columns_lines)

    @property
    def _columns_lines(self):
        columns_lines = []
        for column_name in self.columns:
            column_type = self.columns_types_other_than_text.get(column_name, 'TEXT')
            columns_lines.append(
                self.COLUMN_LINE_TEMPLATE.format(quoted_column_name=quoted(column_name),
                                                 column_type=column_type)
            )
        return ',\n'.join(columns_lines)


if __name__ == '__main__':
    table_ddl = TableDDL(name='my_table',
                         columns=['my_table_id', 'text_column', 'date_column'],
                         columns_types_other_than_text={'my_table_id': 'INTEGER', 'date_column': 'DATE'}
                         )
    print(table_ddl.create_ddl)
    print(table_ddl.drop_ddl)
