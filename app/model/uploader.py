import os
from typing import Dict
from util.utils import comma_separated_columns, create_postgres_engine, get_csv_columns, execute_text_query
from model.ddl import TableDDL
import logging
import pandas as pd

logger = logging.getLogger(__name__)

class Uploader:
    def __init__(self, csv_directory, csv_name, separator,
                 table_name=None,
                 columns_with_non_default_type: Dict[str, str] = {
                     "activites_publiees": "boolean",
                     "exercice_sans_CA": "boolean",
                     "annee_fin": "integer",
                     "action_menee_en_propre": "numeric"
                 },
                 engine=None,
                 ):
        self.csv_directory = csv_directory
        self.csv_name = csv_name
        self.separator = separator
        self.csv_path = os.path.join(csv_directory, csv_name)
        self.table_name = table_name or csv_name[:-4]
        self.columns_with_non_default_type = columns_with_non_default_type or dict()
        self.engine = engine or create_postgres_engine()

    @property
    def columns(self):
        return get_csv_columns(self.csv_path, separator=self.separator)

    @property
    def table_ddl(self):
        return TableDDL(self.table_name, self.columns, self.columns_with_non_default_type)

    def reset(self):
        execute_text_query(self.engine, self.table_ddl.drop_ddl)

    def run(self):
        execute_text_query(self.engine, self.table_ddl.create_ddl)
        self._copy_csv_to_table()

    def _copy_csv_to_table(self):
        logger.info("Load csv '{}' into table '{}'".format(self.csv_path, self.table_name))
        connection = self.engine.raw_connection()
        with connection.cursor() as cursor:
            cursor.execute("SET datestyle = 'ISO,DMY';")
            with open(self.csv_path, 'rb') as csv_file:
                csv_file.readline()
                cursor.copy_expert(
                    """COPY "{table_name}"({columns}) FROM STDIN
                    WITH (FORMAT CSV, DELIMITER '{delimiter}', ENCODING utf8)""".format(
                        table_name=self.table_name,
                        columns=comma_separated_columns(self.columns),
                        delimiter=self.separator
                    ),
                    csv_file
                )
        connection.commit()

    def _inject_csv_to_table(self):
        logger.info("Load csv '{}' into table '{}'".format(self.csv_path, self.table_name))
        df = pd.read_csv(self.csv_path, delimiter=";", low_memory=False)
        df.to_sql(self.table_name, self.engine, if_exists="replace")
        logger.info("Done loading csv '{}' into table '{}'".format(self.csv_path, self.table_name))
