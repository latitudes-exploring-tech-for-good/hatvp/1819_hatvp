## Installation du projet

Les instructions ci-dessous vous permettrons d'obtenir une copie du projet exécutable sur votre machine à des fins de développements, de tests ou pour faire vos propres visualisations des données du répertoire.

## Cloner le projet
```
git clone git@lab.hatvp.fr:latitudes/repertoire.git
cd repertoire
```

## Dépendances

- Python > 3.5
- pip3
- virtualenv (recommandé)
- Docker (pour charger les données dans PostgreSQL et les visualiser dans Metabase)

Tests pour vérifier les dépendances
```
python3 --version
pip3 --version
virtualenv --version
Docker --version
```

## Installation

Création et activation d'un environnement virtuel

```
virtualenv venv --python=python3
source venv/bin/activate
```

Installation des dépendances python
```
pip3 install -r requirements.txt
```

## Traitement des données du répertoire

Lancement du script
```
cd app
python main.py
```

## Démarrage de PostgreSQL et de Metabase via Docker
 
````
docker-compose up -d
````

Voir la documentation de Docker-Compose pour plus de fonctionnalité 

Il reste à charger les données dans PostgreSQL, et configurer Metabase en local

