
*This project aims to facilitate the access to the [Register for interest representatives](https://www.hatvp.fr/le-repertoire/) 
published by The High Authority for Transparency in Public Life*

# Installation

## Clone project 

Execute the following commands in a terminal 

    git clone git@lab.hatvp.fr:latitudes/repertoire.git
    cd repertoire
    
## Python data processing 

We recommend to manage dependencies with `virtualenv`.

**Requirements**
- Python 3.5, 3.6 or 3.7
    - test with `python3 --version`
- pip3
    - test with `pip3 --version`
- virtualenv
    - test with `virtualenv --version`
    - install with `pip3 install virtualenv`

**Installation** 

Tips : if you an Python IDE such as PyCharm, use it directly create the virtualenv and install the dependencies.  

Create virtual environment and activate it.

    virtualenv venv --python=python3
    source venv/bin/activate

Install Python dependencies 

    pip3 install -r requirements.txt
    
# Usage

**Activation** 

If not already done, activate the virtualenv. 

    source venv/bin/activate
    
  
The entrypoint to the Python will be the script `main.py`. 
