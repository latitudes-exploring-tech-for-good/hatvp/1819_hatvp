# AGORA EXPLORER

[English Version](README_EN.md)

Depuis le 1er juillet 2017, le répertoire numérique des représentants d’intérêts, géré par la Haute Autorité pour la transparence de la vie publique, encadre les pratiques de lobbying en France et permet de savoir qui influence qui, sur quel sujet et par quels moyens.

Les 12 derniers mois ont permis la collecte et la publication en open data, sur le site de la Haute Autorité ([https://www.hatvp.fr/le-repertoire/](https://www.hatvp.fr/le-repertoire/)), d’une quantité importante de données sur l’identité et les activités des représentants d’intérêts.

L’un des enjeux majeurs est désormais que ces données soient consultées, travaillées et réutilisées. Toutefois, face à la complexité du dispositif, il est essentiel d’offrir aux citoyens des outils qui leur permettent de s’approprier facilement le répertoire.

Le présent projet est le fruit d’une réflexion autour des leviers d’enrichissement des données du répertoire et leurs perspectives de réutilisation qui a notamment été marquée par la tenue, dans les locaux de la Haute Autorité le 24 mai 2018, du deuxième forum « Open d’Etat » consacré à ces sujets (cf. [https://www.hatvp.fr/presse/opengov-la-haute-autorite-accueille-le-2eme-forum-open-detat-sur-le-repertoire-des-representants-dinterets/](https://www.hatvp.fr/presse/opengov-la-haute-autorite-accueille-le-2eme-forum-open-detat-sur-le-repertoire-des-representants-dinterets/))

Présentation finale du projet : https://www.slideshare.net/ThibaudAschbacher/data-visualisations-sur-les-donnes-des-reprsentants-dintrts-lobbies-en-france/

## Données utilisées

La Haute Autorité fournit, pour chaque représentant d’intérêts enregistré sur le répertoire, un fichier au format .JSON comprenant l’ensemble des informations déclarées (historique des mises à jour inclus).

Exemple : [Fiche Transparency International France](https://www.hatvp.fr/agora/425138393.json)

La Haute Autorité met également à disposition un fichier unique au format .JSON consolidant l’ensemble des données actualisées du répertoire afin d’accorder une plus grande latitude aux réutilisateurs. Ce fichier est mis à jour chaque nuit.

Ces informations sont publiées sous la  [licence ouverte](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf)  Etalab.

-   [Télécharger les données du répertoire (.JSON)](http://www.hatvp.fr/agora/opendata/agora_repertoire_opendata.json)
-   [Télécharger la structure du fichier .JSON (.XLSX)](https://www.hatvp.fr/wordpress/wp-content/uploads/2018/10/descriptif_JSON_AGORA.xlsx)

## Opération réalisées

Le projet commence par télécharger le json à jour.

Il fonctionne ensuite en plusieurs étapes :

1. Mise à plat du json en dataframe et rassemblement des dataframes dans un dictionnaire
2. Nettoyage des dataframes du dictionnaire obtenu 
    - renommage des csv et des colonnes
    - reclassification de certains champs
3. Jointure simple des tables nettoyées 
4. Jointure avancée des tables nettoyées 

## Utilisation du projet

L'installation et l'utilisation du projet sont décrit dans le fichier [INSTALL.md](INSTALL.md).

## Contribuer

Merci de lire le fichier [CONTRIBUTING.md](CONTRIBUTING.md) pour obtenir plus d'informations sur les modalités de contribution au projet.

## Licence

Ce projet est disponible sous licence - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations